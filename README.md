# ITT3-EP-GYTIS

To-Do List for Extended Programming ITT3 by Gytis Valatkevicius  
UCL University College Lillebælt

Some basic folder structure will remain here.
* Report folder contains the hand-in report.
* ToDo_List folder contains the code and files of the task list.
* Info contains some brief explanations of modules.

I made this project as a learning project with some inspiration from TokyoEdtech and his youtube video. All sources included.

## Learning OOP project

I made this small project to learn the basics of OOP and to make a useful daily to-do list that I can use and improve further.  

It is necessary in my opinion to have a ToDo list of some sorts as it simplifies the view and control of everyday tasks, it is easier to follow the tasks you write down than it is to remember all the tasks you set out to do daily.

Overall the project has taught me:
* the use of classes and objects defined in them,
* TkInter module GUI coding,
* saving/loading files with pickle module,
* OOP principles and use,
* S.O.L.I.D principles,
* Microservice use.

### Sources

Video and starting code from wynand1004 aka TokyoEdtech:
* https://www.youtube.com/watch?v=8qUJ9a_3zSQ
* https://github.com/wynand1004/Projects/tree/master/To-Do-List
* https://www.youtube.com/watch?v=K7jOtVpEFLk&list=PLlEgNdBJEO-nn2R-VhaS81jTZUpbpMf0v&index=1

* https://www.educative.io/blog/object-oriented-programming

* https://wiki.python.org/moin/TkInter

* https://wiki.python.org/moin/UsingPickle