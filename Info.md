# Brief info of the modules I used and what-not, with sources included, if I missed sources in the description of the project.

## OOP explained
Object Oriented programming (OOP) is a programming paradigm that relies on the concept of classes and objects.
It is used to structure a software program into simple, reusable pieces of code blueprints (usually called classes), which are used to create individual instances of objects.  
A class is an abstract blueprint used to create more specific, concrete objects.
Classes often represent broad categories, like Car or Dog that share attributes.
These classes define what attributes an instance of this type will have, like color, but not the value of those attributes for a specific object.  
Classes can also contain functions, called methods available only to objects of that type.
These functions are defined within the class and perform some action helpful to that specific type of object.

Source: https://www.educative.io/blog/object-oriented-programming

## TkInter
Tkinter is Python's de-facto standard GUI (Graphical User Interface) package.
It is a thin object-oriented layer on top of Tcl/Tk.
Tkinter is not the only GuiProgramming toolkit for Python. It is however the most commonly used one.

Source: https://wiki.python.org/moin/TkInter

## Pickle
Flying Pickle Alert!
Pickle files can be hacked. If you receive a raw pickle file over the network, don't trust it!
It could have malicious code in it, that would run arbitrary python when you try to de-pickle it.
However, if you are doing your own pickle writing and reading, you're safe. (Provided no one else has access to the pickle file, of course.)

What can you Pickle?
Generally you can pickle any object if you can pickle every attribute of that object.
Classes, functions, and methods cannot be pickled -- if you pickle an object,
the object's class is not pickled, just a string that identifies what class it belongs to.
This works fine for most pickles (but note the discussion about long-term storage of pickles).

Source: https://wiki.python.org/moin/UsingPickle