# A simple To-Do list made using tkinter

# Made by Gytis Valatkevicius following a guide of TokyoEdtech
# Source: https://github.com/wynand1004/Projects/blob/master/To-Do-List/to_do_list.py
# Guide by TokyoEdtech: https://www.youtube.com/watch?v=8qUJ9a_3zSQ

# My Project's gitlab: https://gitlab.com/gytis.valatkevicius/itt3-ep-gytis

# Features a To-Do List with task control, saving and loading tasks, a simple GUI,
# highly configurable with basic knowledge, I added classes and a lot of comments for code.

import sys
# For GUI
import tkinter
import tkinter.messagebox
# For saving tasks
import pickle

# Make a root window for the todo list
root = tkinter.Tk()
root.title("To-Do List")
root.configure(bg="white")
root.geometry("400x600")

# Create an empty list
tasks = []

# List control class
class control:
	def update_list():
		for task in tasks:
			listbox_tasks.insert("end", task)
			print("List updated")
	
	def clear():
		confirmed = tkinter.messagebox.askyesno(title="Warning!", message="Do you want to delete all tasks?")
		if confirmed == True:
			listbox_tasks.delete(0, "end")
			print("List cleared")
		else:
			pass
		
	def exit():
		confirmed = tkinter.messagebox.askyesno(title="Warning!", message="Do you want to exit without saving?")
		if confirmed == True:
			sys.exit("Exiting by request")
		else:
			pass
		
# Task class and methods
class task:
	def add():
		task = entry_task.get()
		if task != "":
			listbox_tasks.insert(tkinter.END, task)
			entry_task.delete(0, tkinter.END)
			print("Adding a task")
		else:
			tkinter.messagebox.showwarning(title="Warning, unintentioned use.", message="You need to enter a task.")
			print("Adding a task failed")
			
	def delete():
		try:
			# Remove the currently selected item
			task_index = listbox_tasks.curselection()[0]
			listbox_tasks.delete(task_index)
			print("Deleting a task")
		except:
			tkinter.messagebox.showwarning(title="Warning, unintentioned use.", message="You need to select a task.")
			print("Deleting a task failed")
	
# GUI control class
class data:
    def load():
        try:
            tasks = pickle.load(open("saved_tasks.dat", "rb"))
            listbox_tasks.delete(0, tkinter.END)
            for task in tasks:
                listbox_tasks.insert(tkinter.END, task)
            print("Loaded tasks succesfully")
        except:
            tkinter.messagebox.showwarning(title="Warning, unintentioned use.", message="Loading of the tasks has failed! Cannot find tasks.dat")
            print("Loading tasks failed")
            
    def save():
        try:
            tasks = listbox_tasks.get(0, listbox_tasks.size())
            # Write tasks to file saved_tasks.dat in root directory
            pickle.dump(tasks, open("saved_tasks.dat", "wb"))
            print("Saved tasks successfuly")
        except:
            tkinter.messagebox.showwarning(title="Warning, unintentioned use.", message="Saving of the tasks has failed!")
            print("Saving tasks failed")
            




# Create a GUI for the todo list
# Description
label_tasks = tkinter.Label(root, text="To-Do List", bg="white")
label_tasks.pack()

label_tasks = tkinter.Label(root, text="Made by Gytis Valatkevicius", bg="white")
label_tasks.pack()

label_tasks = tkinter.Label(root, text="Version: v2", bg="white")
label_tasks.pack()

# Frame
frame_tasks = tkinter.Frame(root)
frame_tasks.pack()

# Listbox
listbox_tasks = tkinter.Listbox(frame_tasks, height=10, width=60)
listbox_tasks.pack(side=tkinter.LEFT)

# Scrollbar
scrollbar_tasks = tkinter.Scrollbar(frame_tasks)
scrollbar_tasks.pack(side=tkinter.RIGHT, fill=tkinter.Y)

listbox_tasks.config(yscrollcommand=scrollbar_tasks.set)
scrollbar_tasks.config(command=listbox_tasks.yview)

# Entry window
entry_task = tkinter.Entry(root, width=60, bg="#E5E5E5")
entry_task.pack()

# Task buttons
label_tasks = tkinter.Label(root, text="Task buttons", bg="#BFBFBF")
label_tasks.pack()

button_add_task = tkinter.Button(root, text="Add task", fg="green", bg="white", width=20, command=task.add)
button_add_task.pack()

button_delete_task = tkinter.Button(root, text="Delete task", fg="red", bg="white", width=20, command=task.delete)
button_delete_task.pack()

#button_up_task = tkinter.Button(root, text="Raise task priority", fg="green", bg="white", width=20, command=task.up)
#button_up_task.pack()

#button_down_task = tkinter.Button(root, text="Lower task priority", fg="red", bg="white", width=20, command=task.down)
#button_down_task.pack()

# Control buttons
label_tasks = tkinter.Label(root, text="Control buttons", bg="#BFBFBF")
label_tasks.pack()

button_load_tasks = tkinter.Button(root, text="Load tasks", fg="green", bg="white", width=20, command=data.load)
button_load_tasks.pack()

button_save_tasks = tkinter.Button(root, text="Save tasks", fg="green", bg="white", width=20, command=data.save)
button_save_tasks.pack()

button_update_tasks = tkinter.Button(root, text="Update tasks", fg="#90EE90", bg="white", width=20, command=control.update_list)
button_update_tasks.pack()

button_clear_task = tkinter.Button(root, text="Remove all tasks", fg="red", bg="white", width=20, command=control.clear)
button_clear_task.pack()

button_exit_task = tkinter.Button(root, text="Exit task list", fg="#BFBFBF", bg="white", width=20, command=control.exit)
button_exit_task.pack()

# Loads the current saved_tasks.dat
load = data.load()
load
# Mainloop for running the list
root.mainloop()
